# Waypoints

A hard fork of [rater193's waypointmanager mod](https://github.com/rater193/Wurm-Unlimited-Waypoints) to fix it up for the latest release as well as add some new features

# How to use

In your Escape menu you can activate and deactivate the Waypoint Manager window.

Right click inside the window to create a new waypoint.

Right click on a waypoint either on the screen or inside the manager to change its visibility or remove it.
