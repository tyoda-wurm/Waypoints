package com.wurmonline.client.renderer.gui;

import com.hypercore.mods.waypoints.Waypoint;
import com.hypercore.mods.waypoints.WaypointMod;
import com.wurmonline.client.renderer.*;
import com.wurmonline.client.renderer.backend.Primitive;
import com.wurmonline.client.renderer.backend.Queue;
import com.wurmonline.client.renderer.backend.VertexBuffer;
import com.wurmonline.client.resources.textures.ResourceTextureLoader;
import com.wurmonline.client.resources.textures.Texture;
import com.wurmonline.client.resources.textures.TextureLoader;
import com.wurmonline.client.util.BufferUtil;
import org.lwjgl.util.glu.GLU;

import java.nio.FloatBuffer;
import java.util.List;

public class WaypointComponent extends WurmComponent {

    private final Matrix modelMatrix;
    private final VertexBuffer vbo;
    private final Waypoint waypoint;

    public WaypointComponent(Waypoint waypoint) {
        super((short)0, "waypoint", 0, 0, waypoint.getSize(), waypoint.getSize());
        this.x = -width;
        this.y = -height;
        this.waypoint = waypoint;
        this.modelMatrix = new Matrix();
        this.vbo = VertexBuffer.create(VertexBuffer.Usage.GUI, 6, true, false, false, false, false, 2, 0, false, true);
        final FloatBuffer vbuffer = this.vbo.lock();
        vbuffer.put(new float[]{-0.5f, -0.5f, 0.0f, 0.0f, 0.0f});
        vbuffer.put(new float[]{-0.5f, 0.5f, 0.0f, 0.0f, 0.5f});
        vbuffer.put(new float[]{0.5f, -0.5f, 0.0f, 0.5f, 0.0f});
        vbuffer.put(new float[]{0.5f, 0.5f, 0.0f, 0.5f, 0.5f});
        vbuffer.put(new float[]{-0.5f, -0.5f, 0.0f, 0.0f, 0.5f});
        vbuffer.put(new float[]{-0.5f, 0.5f, 0.0f, 0.0f, 1.0f});
        this.vbo.unlock();
    }

    @Override
    public void gameTick() {
        if(waypoint.isVisible()) {
            final int[] viewport = {0, 0, hud.getWidth(), hud.getHeight()};
            final FloatBuffer mvBuf = FloatBuffer.wrap(Frustum.mainFrustum.modelviewMatrix);
            final FloatBuffer pBuf = FloatBuffer.wrap(Frustum.mainFrustum.projectionMatrix);
            final float[] poss = new float[3];
            GLU.gluProject(waypoint.getPosX(), waypoint.getPosY(), waypoint.getPosZ(), mvBuf, pBuf, BufferUtil.wrap(viewport), BufferUtil.floatBuffer(3));
            BufferUtil.unwrap(poss);
            if (poss[2] < 1) {
                this.x = (int) poss[0] - width / 2;
                this.y = hud.getHeight() - (int) poss[1] - height / 2;
            } else {
                this.x = -width;
                this.y = -height;
            }
        } else {
            this.x = -width;
            this.y = -height;
        }
    }

    @Override
    protected void renderComponent(final Queue queue, final float alpha) {
        final Texture texture = ResourceTextureLoader.getResidentTexture("img.texture.gui.compass.small", TextureLoader.Filter.LINEAR, false, false);
        Primitive p = queue.reservePrimitive();
        p.copyStateFrom(Renderer.stateAlphaBlend);
        p.setColor(waypoint.getColor());
        p.program = null;
        p.vertex = this.vbo;
        p.index = null;
        p.type = Primitive.Type.TRIANGLESTRIP;
        p.num = 2;
        p.lightManager = null;
        p.texture[0] = texture;
        p.texture[1] = null;
        p.texenv[0] = Primitive.TexEnv.MODULATE;
        p.texturematrix = null;
        p.offset = 0;
        p.clipRect = HeadsUpDisplay.scissor.getCurrent();
        this.modelMatrix.fromTranslationAndNonUniformScale((float)(this.x + this.width / 2), (float)(this.y + this.height / 2), 0.0f, (float)this.width, (float)this.height, 0.0f);
        queue.queue(p, this.modelMatrix);
    }

    @Override
    protected void rightPressed(final int xMouse, final int yMouse, final int clickCount) {
        final WurmPopup popup = new WurmPopup("compassMenu", "Options", xMouse, yMouse);
        popup.addSeparator();
        popup.addButton(popup.new WPopupLiveButton("Remove") {
            @Override
            protected void handleLeftClick() {
                List<WurmComponent> components = hud.getComponents();
                for(int i = 0; i < components.size(); ++i) {
                    WurmComponent component = components.get(i);
                    if(component instanceof WindowWaypointManager) {
                        ((WindowWaypointManager) component).removeWaypoint(waypoint);
                    }
                    if(component instanceof WaypointComponent && ((WaypointComponent) component).getWaypoint() == waypoint) {
                        WurmComponent.hud.removeComponent(component);
                        --i;
                    }
                }
                waypoint.delete();
            }
        });
        popup.addButton(popup.new WPopupLiveButton(String.format("Turn %s", waypoint.isVisible() ? "invisible" : "visible")) {
            @Override
            protected void handleLeftClick() {
                waypoint.setVisible(!waypoint.isVisible());
            }
        });
        CompassComponent.hud.showPopupComponent(popup);
    }

    @Override
    public void pick(final PickData pickData, final int xMouse, final int yMouse) {
        pickData.addText(waypoint.getName());
        int distance = (int)Math.sqrt(Math.pow(waypoint.getPosX() - hud.getWorld().getPlayerPosX(), 2) + Math.pow(waypoint.getPosZ() - hud.getWorld().getPlayerPosY(), 2));
        if (WaypointMod.isShowDistPosTiles())
            distance /= 4;
        pickData.addText("distance: "+distance);
    }

    @Override
    public void setLocation(int newX, int newY, int newWidth, int newHeight) {
        this.setLocation2(newX, newY, newWidth, newHeight);
    }

    @Override
    public WurmComponent getComponentAt(int xMouse, int yMouse) {
        return this.contains(xMouse, yMouse) ? this : null;
    }

    @Override
    boolean isAvailable() {
        return true;
    }

    public Waypoint getWaypoint() {
        return waypoint;
    }
}
