package com.wurmonline.client.renderer.gui;

public class WaypointsTreeList extends WurmTreeList<WaypointsTreeListItem>{
    private int sortColumn, sortOrder;

    WaypointsTreeList(String _name, int[] colWidths, String[] colNames) {
        super(_name, colWidths, colNames);
        sortColumn = 1;
        sortOrder = -1;

    }

    @Override
    public void buttonClicked(WButton button) {
        if (button instanceof WurmTreeList.TreeListButton) {
            this.setSort(((WurmTreeList.TreeListButton) button).index);
        }
    }


    @Override
    protected void rightPressed(int xMouse, int yMouse, int clickCount) {
        final WurmPopup popup = new WurmPopup("treelistMenu", "Options", xMouse, yMouse);
        popup.addSeparator();
        popup.addButton(popup.new WPopupLiveButton("New Waypoint") {
            @Override
            protected void handleLeftClick() {
                WindowNewWaypoint.createWindow();
            }
        });
        WurmComponent.hud.showPopupComponent(popup);
    }

    void reSort() {
        getNode(null).sortOn(sortColumn, sortOrder);
        recalcLines();
    }

    void setSort(int newSort) {
        if (sortColumn == newSort) {
            sortOrder = -sortOrder;
        } else {
            sortColumn = newSort;
            sortOrder = 1;
        }
        reSort();
    }
}
