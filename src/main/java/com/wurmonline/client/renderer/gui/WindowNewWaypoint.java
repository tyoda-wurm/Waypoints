package com.wurmonline.client.renderer.gui;

import com.hypercore.mods.waypoints.Waypoint;
import com.hypercore.mods.waypoints.WaypointMod;

import java.util.logging.Level;

public class WindowNewWaypoint extends WWindow implements InputFieldListener  {
	public static final String panelName = "New Waypoint";

	public WindowNewWaypoint(float posX, float posY, float posZ) {
		super(panelName, true);
		setTitle(panelName);
		WurmBorderPanel mainPanel = new WurmBorderPanel(panelName);
		
		WurmArrayPanel<FlexComponent> elements = new WurmArrayPanel<>("Waypoint Manager Buttons", WurmArrayPanel.DIR_VERTICAL);
		elements.setInitialSize(32, 256, false);
		
		WindowNewWaypoint instance = this;

		WurmInputField ifName = new WurmInputField("Name", this);
		ifName.prompt = "Name: ";
		ifName.setText("Waypoint");

		WurmInputField ifSize = new WurmInputField("Size", this);
		ifSize.prompt = "Size: ";
		ifSize.setText(Integer.toString(WaypointMod.getDefaultSize()));

		WurmLabel sizeLabel = new WurmLabel("Coordinates:", (WaypointMod.isShowDistPosTiles() ? "XZ position in tiles, Y in meters" : "XYZ position in meters"), false);

		WurmInputField ifPosX = new WurmInputField("X", this);
		ifPosX.prompt = "X: ";
		ifPosX.setText(Float.toString(posX));

		WurmInputField ifPosY = new WurmInputField("Y", this);
		ifPosY.prompt = "Y: ";
		ifPosY.setText(Float.toString(posY));

		WurmInputField ifPosZ = new WurmInputField("Z", this);
		ifPosZ.prompt = "Z: ";
		ifPosZ.setText(Float.toString(posZ));

		WurmLabel colorLabel = new WurmLabel("RGBA Color:", "Set alpha to 0 to disable the color", false);

		WurmInputField ifColorR = new WurmInputField("R", this);
		ifColorR.prompt = "R (0-255): ";
		ifColorR.setText(Integer.toString(WaypointMod.getDefaultColorR()));

		WurmInputField ifColorG = new WurmInputField("G", this);
		ifColorG.prompt = "G (0-255): ";
		ifColorG.setText(Integer.toString(WaypointMod.getDefaultColorG()));

		WurmInputField ifColorB = new WurmInputField("B", this);
		ifColorB.prompt = "B (0-255): ";
		ifColorB.setText(Integer.toString(WaypointMod.getDefaultColorB()));

		WurmInputField ifColorA = new WurmInputField("A", this);
		ifColorA.prompt = "A (0-1.0): ";
		ifColorA.setText(Float.toString(WaypointMod.getDefaultColorA()));

		WButton btnCreateWaypoint = new WButton("Create Waypoint") {
			@Override
			protected void leftPressed(int xMouse, int yMouse, int clickCount) {
				super.leftPressed(xMouse, yMouse, clickCount);
				try {
					String name = ifName.getText();
					name = name.isEmpty() ? "Waypoint" : name;
					int size = Integer.parseInt(ifSize.getText());

					float x = Float.parseFloat(ifPosX.getText());
					float y = Float.parseFloat(ifPosY.getText());
					float z = Float.parseFloat(ifPosZ.getText());
					if (WaypointMod.isShowDistPosTiles()) {
						x *= 4;
						z *= 4;
					}

					float r = Integer.parseInt(ifColorR.getText()) / 255.0f;
					float g = Integer.parseInt(ifColorG.getText()) / 255.0f;
					float b = Integer.parseInt(ifColorB.getText()) / 255.0f;
					float a = Float.parseFloat(ifColorA.getText());

					Waypoint waypoint = Waypoint.createWaypoint(x, y, z, name, size, r, g, b, a);

					hud.toggleComponent(instance);
					for(WurmComponent component : hud.getComponents()) {
						if(component instanceof WindowWaypointManager) {
							((WindowWaypointManager) component).addWaypoint(waypoint);
							WaypointMod.logger.info("Added new waypoint hud element");
							break;
						}
					}
				}catch(Exception e) {
					WaypointMod.logger.log(Level.SEVERE, "Could not create waypoint: ", e);
				}
			}
		};
		btnCreateWaypoint.setPosition(4, 4);
		btnCreateWaypoint.setSize(248, 24);

		elements.addComponent(ifName);
		elements.addComponent(ifSize);

		elements.addComponent(sizeLabel);
		elements.addComponent(ifPosX);
		elements.addComponent(ifPosY);
		elements.addComponent(ifPosZ);

		elements.addComponent(colorLabel);
		elements.addComponent(ifColorR);
		elements.addComponent(ifColorG);
		elements.addComponent(ifColorB);
		elements.addComponent(ifColorA);

		elements.addComponent(btnCreateWaypoint);
		
		mainPanel.setComponent(elements, WurmBorderPanel.CENTER);
		
		setComponent(mainPanel);
		setInitialSize(254, 216, false);
		this.resizable = true;
		layout();
		
		sizeFlags = FlexComponent.FIXED_WIDTH;
		this.closeable = true;
	}

	public static void createWindow() {
		float x = WaypointMod.world.getWorldRenderer().getCameraX();
		float y = WaypointMod.world.getWorldRenderer().getCameraY();
		float z = WaypointMod.world.getWorldRenderer().getCameraZ() + 1.5f;
		if(WaypointMod.isShowDistPosTiles()) {
			x /= 4.0f;
			z /= 4.0f;
		}
		hud.toggleComponent(new WindowNewWaypoint(x, y, z));
	}

	@Override
	void closePressed() {
		hud.toggleComponent(this);
	}

	@Override
	public void handleInput(String p0) {

	}

	@Override
	public void handleInputChanged(WurmInputField p0, String p1) {

	}

	@Override
	public void handleEscape(WurmInputField wif) {

	}
}
