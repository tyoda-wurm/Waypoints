package com.wurmonline.client.renderer.gui;

import com.hypercore.mods.waypoints.Waypoint;
import com.hypercore.mods.waypoints.WaypointMod;
import com.wurmonline.client.renderer.gui.text.TextFont;

import java.util.*;

public class WindowWaypointManager extends WWindow {
	public static String panelName = "Waypoint Manager";

	private final WaypointsTreeList list;
	private final Map<Waypoint, WaypointsTreeListItem> items;

	public WindowWaypointManager() {
		super(panelName, true);
		setTitle(panelName);

		this.x = WaypointMod.getDefaultWindowX();
		this.y = WaypointMod.getDefaultWindowY();
		this.width = WaypointMod.getDefaultWindowWidth();
		this.height = WaypointMod.getDefaultWindowHeight();

		final int colWidthVisible = TextFont.getText().getWidth("XVisibleX");
		final int colWidthNumber = TextFont.getText().getWidth("X999999X");
		final int[] colWidths;
		final String[] colNames;
		if(WaypointMod.isManagerShowPos()) {
			colWidths = new int[]{colWidthVisible, colWidthNumber, colWidthNumber, colWidthNumber};
			colNames = new String[]{"Visible", "X", "Y", "Z"};
		} else {
			colWidths = new int[]{colWidthVisible};
			colNames = new String[]{"Visible"};
		}

		items = new HashMap<>();

		list = new WaypointsTreeList("GainTrackerTree", colWidths, colNames);

		hud.showComponent(this);

		setComponent(list);
		this.closeable = true;

		for(Waypoint waypoint : Waypoint.getWaypoints()) {
			addWaypoint(waypoint);
		}
	}

	@Override
	void closePressed() {
		hud.toggleComponent(this);
	}

	@Override
	public void setSize(int newWidth, int newHeight) {
		super.setSize(newWidth, newHeight);
		WaypointMod.setWindowPreferences(this.x, this.y, width, height);
	}

	@Override
	public void setPosition(int newX, int newY) {
		super.setPosition(newX, newY);
		WaypointMod.setWindowPreferences(this.x, this.y, width, height);
	}

	public void addWaypoint(Waypoint waypoint) {
		hud.toggleComponent(new WaypointComponent(waypoint));
		if(!items.containsKey(waypoint)) {
			WaypointsTreeListItem item = new WaypointsTreeListItem(waypoint);
			items.put(waypoint, item);
			list.addTreeListItem(item, null);
			list.reSort();
		}
	}

	public void removeWaypoint(Waypoint waypoint) {
		WaypointMod.logger.info("cheese");
		if(items.containsKey(waypoint)) {
			WaypointMod.logger.info("cheese2");
			list.removeTreeListItem(items.remove(waypoint));
			list.reSort();
		}
	}
}
