package com.wurmonline.client.renderer.gui;

import com.hypercore.mods.waypoints.Waypoint;
import com.hypercore.mods.waypoints.WaypointMod;
import com.wurmonline.client.renderer.PickData;

import java.util.List;

public class WaypointsTreeListItem extends TreeListItem {
    Waypoint waypoint;

    WaypointsTreeListItem(Waypoint waypoint) {
        this.waypoint = waypoint;
    }

    @Override
    String getName() {
        return waypoint.getName();
    }

    @Override
    void rightClick(int xMouse, int yMouse) {
        final WurmPopup popup = new WurmPopup("treelistMenu", "Options", xMouse, yMouse);
        popup.addSeparator();
        popup.addButton(popup.new WPopupLiveButton("Remove") {
            @Override
            protected void handleLeftClick() {
                List<WurmComponent> components = hud.getComponents();
                for(int i = 0; i < components.size(); ++i) {
                    WurmComponent component = components.get(i);
                    if(component instanceof WindowWaypointManager) {
                        ((WindowWaypointManager) component).removeWaypoint(waypoint);
                    }
                    if(component instanceof WaypointComponent && ((WaypointComponent) component).getWaypoint() == waypoint) {
                        WurmComponent.hud.removeComponent(component);
                        --i;
                    }
                }
                waypoint.delete();
            }
        });
        popup.addButton(popup.new WPopupLiveButton(String.format("Turn %s", waypoint.isVisible() ? "invisible" : "visible")) {
            @Override
            protected void handleLeftClick() {
                waypoint.setVisible(!waypoint.isVisible());
            }
        });
        popup.addButton(popup.new WPopupLiveButton("New Waypoint") {
            @Override
            protected void handleLeftClick() {
                WindowNewWaypoint.createWindow();
            }
        });
        WurmComponent.hud.showPopupComponent(popup);
    }

    @Override
    public void getHoverDescription(PickData pd) {
        super.getHoverDescription(pd);
        pd.addText(waypoint.getName());
    }

    @Override
    int compareTo(TreeListItem o, int i) {
        if (o instanceof WaypointsTreeListItem) {
            return waypoint.getName().compareTo(((WaypointsTreeListItem) o).waypoint.getName());
        }
        return 0;
    }

    @Override
    String getParameter(int param) {
        switch (param) {
            case 0:
                return waypoint.isVisible() ? "yes" : "no";
            case 1:
                return Integer.toString((int) (WaypointMod.isShowDistPosTiles() ? waypoint.getTileX() : waypoint.getPosX()));
            case 2:
                return Integer.toString((int) waypoint.getPosY());
            case 3:
                return Integer.toString((int) (WaypointMod.isShowDistPosTiles() ? waypoint.getTileZ() : waypoint.getPosZ()));
            default:
                return "";
        }
    }
}

