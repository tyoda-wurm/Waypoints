package com.hypercore.mods.waypoints;

import com.wurmonline.client.renderer.Color;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

public class Waypoint {
	private static ArrayList<Waypoint> waypoints = new ArrayList<>();

	private final float posZ;
	private final float posX;
	private final float posY;
	private final String name;
	private final float r;
	private final float g;
	private final float b;
	private final float a;
	private final int size;
	private boolean visible;

	private Waypoint(float x, float y, float z, String name, int size, float colorR, float colorG, float colorB, float colorA, boolean visible) {
		this.posX = x;
		this.posY = y;
		this.posZ = z;
		this.name = name;
		this.r = colorR;
		this.g = colorG;
		this.b = colorB;
		this.a = colorA;
		this.size = size;
		this.visible = true;
		waypoints.add(this);
	}

	public static Waypoint createWaypoint(float x, float y, float z, String name, int size, float colorR, float colorG, float colorB, float colorA) {
		Waypoint waypoint = new Waypoint(x, y, z, name, size, colorR, colorG, colorB, colorA, true);
		saveWaypoints();
		return waypoint;
	}

	public static void configure() {
		Path waypointsConfigPath = Paths.get(WaypointMod.WAYPOINTS_CONFIG_PATH);
		if(Files.exists(waypointsConfigPath)) {
			try (BufferedReader reader = Files.newBufferedReader(waypointsConfigPath)) {
				Properties config = new Properties();
				config.load(reader);
				int num = 1;
				String id = "waypoint"+num;
				for(; config.containsKey(id+".name"); ++num, id="waypoint"+num) {
					float posX = Float.parseFloat(config.getProperty(id+".posX").trim());
					float posY = Float.parseFloat(config.getProperty(id+".posY").trim());
					float posZ = Float.parseFloat(config.getProperty(id+".posZ").trim());
					float r = Float.parseFloat(config.getProperty(id+".r").trim());
					float g = Float.parseFloat(config.getProperty(id+".g").trim());
					float b = Float.parseFloat(config.getProperty(id+".b").trim());
					float a = Float.parseFloat(config.getProperty(id+".a").trim());
					String name = config.getProperty(id+".name").trim();
					int size = Integer.parseInt(config.getProperty(id+".size").trim());
					boolean visible = Boolean.parseBoolean(config.getProperty(id+".visible").trim());
					new Waypoint(posX, posY, posZ, name, size, r, g, b, a, visible);
				}
			} catch (IOException e) {
				WaypointMod.logger.log(Level.SEVERE, "Could not read waypoints config.", e);
			}
		}
	}

	private static void saveWaypoints() {
		try(Writer writer = Files.newBufferedWriter(Paths.get(WaypointMod.WAYPOINTS_CONFIG_PATH))) {
			Properties config = new Properties();
			int num = 1;
			for(Waypoint waypoint : waypoints) {
				String id = "waypoint"+num;
				config.put(id+".posX", Float.toString(waypoint.posX));
				config.put(id+".posZ", Float.toString(waypoint.posZ));
				config.put(id+".posY", Float.toString(waypoint.posY));
				config.put(id+".name", waypoint.name);
				config.put(id+".r", Float.toString(waypoint.r));
				config.put(id+".g", Float.toString(waypoint.g));
				config.put(id+".b", Float.toString(waypoint.b));
				config.put(id+".a", Float.toString(waypoint.a));
				config.put(id+".size", Integer.toString(waypoint.size));
				config.put(id+".visible", Boolean.toString(waypoint.visible));
				++num;
			}
			config.store(writer, WaypointMod.CONFIG_DISCLAIMER);
		} catch (IOException e) {
			WaypointMod.logger.log(Level.SEVERE, "Could not write waypoints config.", e);
		}
	}

	public void delete() {
		waypoints.remove(this);
		saveWaypoints();
	}

	public static List<Waypoint> getWaypoints() {
		return new ArrayList<>(waypoints);
	}

	public int getSize() {
		return size;
	}

	public float getPosX() {
		return posX;
	}

	public float getPosY() {
		return posY;
	}

	public float getPosZ() {
		return posZ;
	}

	public float getTileX() {
		return posX/4.0f;
	}

	public float getTileY() {
		return posY/4.0f;
	}

	public float getTileZ() {
		return posZ/4.0f;
	}


	public Color getColor() {
		return new Color(r, g, b, a);
	}

	public String getName() {
		return name;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
