package com.hypercore.mods.waypoints;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javassist.*;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.Initable;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;
import org.gotti.wurmunlimited.modsupport.console.ConsoleListener;
import org.gotti.wurmunlimited.modsupport.console.ModConsole;

import com.wurmonline.client.game.World;

public class WaypointMod implements WurmClientMod, Initable, PreInitable, Configurable, ConsoleListener {
	public static final String version = "ty1.1.2";
	public static final Logger logger = Logger.getLogger(WaypointMod.class.getName());
	public static final String MOD_BASE_DIR_PATH = "./mods/Waypoints/";
	public static final String WINDOW_CONFIG_PATH = MOD_BASE_DIR_PATH + "window.config";
	public static final String WAYPOINTS_CONFIG_PATH = MOD_BASE_DIR_PATH + "waypoints.config";
	public static final String CONFIG_DISCLAIMER = "#This file is used to store data about the mod. You can change them manually, but probably shouldn't.";

	/**
	 * Set when the game starts via a preInit hook
	 */
	public static World world = null;

	private static int defaultColorR = 255;
	private static int defaultColorG = 0;
	private static int defaultColorB = 0;
	private static float defaultColorA = 1.0f;
	private static int defaultSize = 40;
	private static int defaultWindowX = 250;
	private static int defaultWindowY = 250;
	private static int defaultWindowWidth = 128;
	private static int defaultWindowHeight = 128;
	private static boolean showDistPosTiles = false;
	private static boolean managerShowPos = false;

	@Override
	public void configure(Properties p) {
		defaultColorR = Integer.parseInt(p.getProperty("defaultColorR", Integer.toString(defaultColorR)).trim());
		defaultColorG = Integer.parseInt(p.getProperty("defaultColorG", Integer.toString(defaultColorG)).trim());
		defaultColorB = Integer.parseInt(p.getProperty("defaultColorB", Integer.toString(defaultColorB)).trim());
		defaultColorA = Float.parseFloat(p.getProperty("defaultColorA", Float.toString(defaultColorA)).trim());
		defaultSize = Integer.parseInt(p.getProperty("defaultSize", Integer.toString(defaultSize)).trim());
		showDistPosTiles = Boolean.parseBoolean(p.getProperty("showDistPosTiles", Boolean.toString(showDistPosTiles)).trim());
		managerShowPos = Boolean.parseBoolean(p.getProperty("managerShowPos", Boolean.toString(managerShowPos)).trim());

		Path windowConfigPath = Paths.get(WINDOW_CONFIG_PATH);
		if(Files.exists(windowConfigPath)) {
			try (BufferedReader reader = Files.newBufferedReader(windowConfigPath)) {
				Properties windowConfig = new Properties();
				windowConfig.load(reader);
				defaultWindowX = Integer.parseInt(windowConfig.getProperty("defaultWindowX", Integer.toString(defaultWindowX)));
				defaultWindowY = Integer.parseInt(windowConfig.getProperty("defaultWindowY", Integer.toString(defaultWindowY)));
				defaultWindowWidth = Integer.parseInt(windowConfig.getProperty("defaultWindowWidth", Integer.toString(defaultWindowWidth)));
				defaultWindowHeight = Integer.parseInt(windowConfig.getProperty("defaultWindowHeight", Integer.toString(defaultWindowHeight)));
				logger.info(String.format("Read window config: %dx, %dy, %dw, %dh", defaultWindowX, defaultWindowY, defaultWindowWidth, defaultWindowHeight));
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Could not read window config.", e);
			}
		}

		Waypoint.configure();
	}

	@Override
	public void preInit() {
		try {
			ClassPool classPool = HookManager.getInstance().getClassPool();

			logger.info("Adding hook to get world.");
			CtClass ctClientBase = classPool.getCtClass("com.wurmonline.client.WurmClientBase");
			ctClientBase.getDeclaredMethod("runGame").insertBefore("com.hypercore.mods.waypoints.WaypointMod.world = this.world;");

			logger.info("Adding hook to HeadsUpDisplay");
			CtClass ctHeadsUpDisplay = classPool.getCtClass("com.wurmonline.client.renderer.gui.HeadsUpDisplay");
			ctHeadsUpDisplay.getDeclaredMethod("init").insertAfter("" +
					"$0.mainMenu.registerComponent(\"Waypoint Manager\", new com.wurmonline.client.renderer.gui.WindowWaypointManager());"
			);
		} catch (NotFoundException | CannotCompileException e) {
			logger.log(Level.SEVERE, "PreInit failed.", e);
			throw new HookException(e);
		}
	}

	@Override
	public void init() {
		logger.log(Level.INFO, "Registering HeadsUpDisplay.init Hook");
		ModConsole.addConsoleListener(this);
	}

	@Override
	public boolean handleInput(String cmd, Boolean arg2) {
		if(cmd.equals("wp")) {
			logger.log(Level.INFO, "Toggling Waypoint Manager Window");
			logger.log(Level.INFO, "There was an error processing your request.");
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public static void setWindowPreferences(int x, int y, int width, int height) {
		try(Writer writer = Files.newBufferedWriter(Paths.get(WINDOW_CONFIG_PATH))) {
			defaultWindowX = x;
			defaultWindowY = y;
			defaultWindowWidth = width;
			defaultWindowHeight = height;
			Properties config = new Properties();
			config.put("defaultWindowX", Integer.toString(x));
			config.put("defaultWindowY", Integer.toString(y));
			config.put("defaultWindowWidth", Integer.toString(width));
			config.put("defaultWindowHeight", Integer.toString(height));
			config.store(writer, CONFIG_DISCLAIMER);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not write window config.", e);
		}
	}

	public static int getDefaultWindowX() {
		return defaultWindowX;
	}

	public static int getDefaultWindowY() {
		return defaultWindowY;
	}

	public static int getDefaultWindowWidth() {
		return defaultWindowWidth;
	}

	public static int getDefaultWindowHeight() {
		return defaultWindowHeight;
	}

	public static int getDefaultColorR() {
		return defaultColorR;
	}

	public static int getDefaultColorG() {
		return defaultColorG;
	}

	public static int getDefaultColorB() {
		return defaultColorB;
	}

	public static float getDefaultColorA() {
		return defaultColorA;
	}

	public static int getDefaultSize() {
		return defaultSize;
	}

	public static boolean isShowDistPosTiles() {
		return showDistPosTiles;
	}

	public static boolean isManagerShowPos() {
		return managerShowPos;
	}

	@Override
	public String getVersion() {
		return version;
	}
}
